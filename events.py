#!/usr/bin/python3

import re

f = open('events.log')
min_count = 0
prev_date = None
prev_time = None

for line in f:
    result = re.search(r'NOK', line)
    if result:
        if prev_date and prev_time:
            date = re.search(r'\d{4}-\d\d-\d\d', line)
            time = re.search(r'\d\d:\d\d', line)
            min_count += 1
            if date[0] != prev_date[0] or time[0] != prev_time[0]:
                print(prev_date[0]+' '+prev_time[0]+' -', min_count, ' NOK')
                prev_date = date
                prev_time = time
                min_count = 0
        else:
            prev_date = re.search(r'\d{4}-\d\d-\d\d', line)
            prev_time = re.search(r'\d\d:\d\d', line)

print(prev_date[0] + ' ' + prev_time[0] + ' -', min_count+1, ' NOK')
f.close()
